import baseData from "../public/data";
import { useState, useEffect } from "react";
import {
  sortIdByRecent,
  createSelfLookUp,
  createLookUp,
  sortByRecent,
  sortByOld
} from "components/utils";
import React from "react";

export const startTime = new Date().getTime();

const setTime = (obj, key) => {
  Object.values(obj).forEach(x => (x[key] = startTime + x[key]));
};

setTime(baseData.users, "checked");
setTime(baseData.posts, "dateTime");
setTime(baseData.comments, "dateTime");
setTime(baseData.commentlikes, "dateTime");
setTime(baseData.postlikes, "dateTime");
setTime(baseData.follows, "dateTime");

let _setUsers;
let _setPosts;
let _setComments;
let _setCommentlikes;
let _setPostlikes;
let _setFollows;
let _setDetails;

let _makePostLike;
let _makeCommentLike;

let _users;
let _posts;
let _comments;
let _commentlikes;
let _postlikes;
let _follows;
let _details;
let _generic = baseData.generic;

let _toUpdate;
let _setToUpdate;
let _makeComment;
let _makePost;
let _makeUser;
let _removePost;
let _removeComment;

let _getUser;
let _getPost;

let _otherUserList;

export default function useData(router) {
  const [users, setUsers] = useState(baseData.users);
  const [posts, setPosts] = useState(baseData.posts);
  const [comments, setComments] = useState(baseData.comments);
  const [commentlikes, setCommentlikes] = useState(baseData.commentlikes);
  const [postlikes, setPostlikes] = useState(baseData.postlikes);
  const [follows, setFollows] = useState(baseData.follows);
  const [details, setDetails] = useState(baseData.details);
  const [toUpdate, setToUpdate] = useState();
  const [otherUserList, setOtherUserList] = useState([]);

  _toUpdate = toUpdate;
  _setToUpdate = setToUpdate;
  _setUsers = setUsers;
  _setPosts = setPosts;
  _setComments = setComments;
  _setCommentlikes = setCommentlikes;
  _setPostlikes = setPostlikes;
  _setFollows = setFollows;
  _setDetails = setDetails;
  _users = users;
  _otherUserList = otherUserList;
  _posts = posts;
  _comments = comments;
  _commentlikes = commentlikes;
  _postlikes = postlikes;
  _follows = follows;
  _details = details;

  const [followsByUser, setFollowsByUser] = useState();
  const [following, setFollowing] = useState([]);
  const [followsByTarget, setFollowsByTarget] = useState();
  const [likesByComment, setLikesByComment] = useState();
  const [postList, setPostList] = useState([]);
  const [postsByUser, setPostsByUser] = useState();
  const [commentsByPost, setCommentsByPost] = useState();
  const [commentsByUser, setCommentsByUser] = useState();
  const [likesByPost, setLikesByPost] = useState();
  const [usersByUserName, setUsersByUserName] = useState();
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    if (
      users &&
      posts &&
      comments &&
      commentlikes &&
      postlikes &&
      follows &&
      details
    ) {
      let commentsByPost = createLookUp(comments, "post", posts, sortByOld);
      let commentsByUser = createLookUp(comments, "user", users);
      let followsByUser = createLookUp(follows, "user", users, false, "target");
      let followsByTarget = createLookUp(
        follows,
        "target",
        users,
        false,
        "user"
      );
      let likesByPost = createLookUp(postlikes, "target", posts, false, "user");
      let likesByComment = createLookUp(
        commentlikes,
        "target",
        comments,
        false,
        "user"
      );
      let postsByUser = createLookUp(posts, "user", users);
      let usersByUserName = createSelfLookUp(users, "userName");

      setCommentsByPost(commentsByPost);
      setCommentsByUser(commentsByUser);
      setFollowsByUser(followsByUser);
      setFollowing(followsByUser[details.currentUser]);
      setFollowsByTarget(followsByTarget);
      setLikesByPost(likesByPost);
      setLikesByComment(likesByComment);
      setPostsByUser(postsByUser);
      setUsersByUserName(usersByUserName);

      let postids = [];
      let listid = details.currentUser + router.asPath;

      if (
        router &&
        router.query &&
        (router.query.userName || router.query.postId)
      ) {
        if (router.query.userName) {
          postids = postsByUser[usersByUserName[router.query.userName]];
        }

        if (router.query.postId) {
          postids = [router.query.postId];
        }
      } else {
        let following = Array.from(
          new Set([...followsByUser[details.currentUser], details.currentUser])
        );
        postids = following.map(user => postsByUser[user]).flat();
      }

      if (!postids) {
        postids = [];
      }

      let getPost = id => ({
        id,
        ...posts[id],
        likes: likesByPost[id],
        liked: likesByPost[id].includes(details.currentUser),
        comments: commentsByPost[id].map(id => ({
          id,
          ...comments[id],
          likes: likesByComment[id],
          liked: likesByComment[id].includes(details.currentUser)
        }))
      });

      _getPost = getPost;

      let arr = postids.sort(sortIdByRecent(posts)).map(getPost);
      arr.listid = listid;

      setPostList(arr);

      setOtherUserList(
        Object.keys(users).filter(x => x - details.currentUser !== 0)
      );
      setNotifications(
        getNotifications(
          postsByUser,
          commentsByUser,
          followsByTarget,
          likesByPost,
          commentsByPost,
          likesByComment,
          users,
          details
        )
      );
    }
  }, [
    users,
    posts,
    comments,
    commentlikes,
    postlikes,
    follows,
    details,
    router,
    router.asPath
  ]);

  const getNotifications = (
    postsByUser,
    commentsByUser,
    followsByTarget,
    likesByPost,
    commentsByPost,
    likesByComment,
    users,
    details
  ) => {
    let likesOnUserPosts = postsByUser[details.currentUser]
      .map(post => likesByPost[post].map(user => `${user}l${post}`))
      .flat();
    let commentsOnUserPosts = postsByUser[details.currentUser]
      .map(post => commentsByPost[post])
      .flat();

    let likesOnUserComments = commentsByUser[details.currentUser]
      .map(comment => likesByComment[comment].map(user => `${user}l${comment}`))
      .flat();

    let userFollows = followsByTarget[details.currentUser].map(
      f => `${f}f${details.currentUser}`
    );

    let allNotifications = [
      ...likesOnUserPosts.map(id => ({
        ...postlikes[id],
        id,
        type: "postLike"
      })),
      ...commentsOnUserPosts.map(id => ({
        ...comments[id],
        id,
        type: "comment"
      })),
      ...likesOnUserComments.map(id => ({
        ...commentlikes[id],
        id,
        type: "commentLike",
        post: comments[commentlikes[id].target].post
      })),
      ...userFollows.map(id => ({ ...follows[id], id, type: "follower" }))
    ]
      .filter(n => n.user - details.currentUser !== 0)
      .sort(sortByRecent);

    let checked = users[details.currentUser].checked;

    let limit = 20;

    let _notifications = [];

    _notifications = allNotifications.slice(0, limit);

    let readIndex = _notifications.findIndex(n => n.dateTime < checked);

    let setUnread = x => {
      x.unread = true;
    };

    let unread = readIndex == -1 ? _notifications.length : readIndex;

    for (let i = 0; i < unread; i++) {
      setUnread(_notifications[i]);
    }

    _notifications.unread = unread;

    return _notifications;
  };

  _makeComment = (comment, key) => {
    if (!key) {
      key = details.lastcomment + 1;
      setDetails(details => ({
        ...details,
        lastcomment: key
      }));
    }

    let defaultComment = {
      user: details.currentUser,
      dateTime: new Date().getTime()
    };

    setComments(comments => ({
      ...comments,
      [key]: { ...defaultComment, ...comment }
    }));
    return key;
  };

  _makePost = (post, key) => {
    if (!key) {
      key = details.lastpost + 1;
      setDetails(details => ({
        ...details,
        lastpost: key
      }));
    }

    let defaultPost = {
      user: details.currentUser,
      dateTime: new Date().getTime()
    };

    setPosts(posts => ({
      ...posts,
      [key]: { ...defaultPost, ...post }
    }));
    return key;
  };

  _removeComment = key => {
    setCommentlikes(c => {
      let newCommentLikes = { ...c };
      likesByComment[key].forEach(k => delete newCommentLikes[k + "l" + key]);
      return newCommentLikes;
    });

    setComments(c => {
      let newComments = { ...c };
      delete newComments[key];
      return newComments;
    });
  };

  let _removePostLeaves = key => {
    let commentsToRemove = commentsByPost[key];

    if (commentsToRemove) {
      let commentlikesToRemove = commentsToRemove
        .map(target => likesByComment[target].map(user => user + "l" + target))
        .flat();

      setCommentlikes(c => {
        let newCommentLikes = { ...c };
        commentlikesToRemove.forEach(k => delete newCommentLikes[k]);
        return newCommentLikes;
      });

      setComments(c => {
        let newComments = { ...c };
        commentsToRemove.forEach(k => delete newComments[k]);
        return newComments;
      });
    }

    let postlikesToRemove = likesByPost[key].map(user => user + "l" + key);

    setPostlikes(c => {
      let newPostlikes = { ...c };
      postlikesToRemove.forEach(k => delete newPostlikes[k]);
      return newPostlikes;
    });
  };

  _removePost = key => {
    _removePostLeaves(key);

    setPosts(posts => {
      let newPosts = { ...posts };
      delete newPosts[key];
      return newPosts;
    });
  };

  _makeUser = (user, key) => {
    if (key === undefined) {
      key = `${details.lastuser + 1}`;
      setDetails(details => ({
        ...details,
        lastuser: key
      }));
    }

    setUsers(users => ({
      ...users,
      [key]: { ...user }
    }));

    return key;
  };

  _makePostLike = like => {
    setPostlikes(l => ({
      ...l,
      [like.key]: { ...like.obj }
    }));
  };

  _makeCommentLike = like => {
    setCommentlikes(l => ({
      ...l,
      [like.key]: { ...like.obj }
    }));
  };

  _getUser = id => users[id];

  return {
    followsByUser,
    followsByTarget,
    likesByComment,
    postList,
    postsByUser,
    commentsByPost,
    likesByPost,
    usersByUserName,
    otherUserList,
    notifications,
    details,
    following,
    users,
    posts,
    comments,
    follows
  };
}

export { _setUsers as setUsers };
export { _setPosts as setPosts };
export { _setComments as setComments };
export { _setCommentlikes as setCommentlikes };
export { _setPostlikes as setPostlikes };
export { _setFollows as setFollows };
export { _setDetails as setDetails };
export { _users as users };
export { _posts as posts };
export { _comments as comments };
export { _commentlikes as commentlikes };
export { _postlikes as postlikes };
export { _follows as follows };
export { _details as details };
export { _generic as generic };
export { _toUpdate as toUpdate };
export { _setToUpdate as setToUpdate };

export { _getUser as getUser };
export { _getPost as getPost };

export { _makeComment as makeComment };
export { _makePost as makePost };
export { _makeUser as makeUser };

export { _removeComment as removeComment };
export { _removePost as removePost };

export { _makeCommentLike as makeCommentLike };
export { _makePostLike as makePostLike };

export { _otherUserList as otherUserList };

export const UserContext = React.createContext({
  current: null,
  following: [],
  toUpdate: false,
  setToUpdate: x => (toUpdate = x)
});
