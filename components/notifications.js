import Link from 'next/link';
import { getUser } from 'components/useData';
import { Scroll } from 'components';

export default function Notifications({ notifications, dismiss }) {
  const types = {
    commentLike: n => (
      <>
        {' liked your '}
        <Link
          legacyBehavior
          href='/post/[postId]'
          as={`/post/${n.post}#comment-${n.target}`}
        >
          <a> comment</a>
        </Link>
      </>
    ),
    postLike: n => (
      <>
        {' liked your '}
        <Link legacyBehavior href='/post/[postId]' as={`/post/${n.target}`}>
          <a> post</a>
        </Link>
      </>
    ),
    follower: n => (
      <>
        {' followed '}
        <Link
          legacyBehavior
          href={`/user/[userName]`}
          as={`/user/${getUser(n.user).userName}`}
        >
          <a> you</a>
        </Link>
      </>
    ),
    comment: n => (
      <>
        {' comented on your '}
        <Link
          legacyBehavior
          href={`/post/[postId]`}
          as={`/post/${n.post}#comment-${n.id}`}
        >
          <a>post</a>
        </Link>
      </>
    )
  };

  const userStr = user => {
    let u = getUser(user);
    return (
      <Link legacyBehavior href='/user/[userName]' as={`/user/${u.userName}`}>
        <a>{u.name}</a>
      </Link>
    );
  };

  const NotificationsList = (
    <div className='notifications-list'>
      {notifications.length ? (
        notifications.map(n => (
          <div
            onClick={dismiss}
            key={`${n.id}${n.type}`}
            className={n.unread ? 'unread' : ''}
          >
            {userStr(n.user)}
            {types[n.type](n)}
          </div>
        ))
      ) : (
        <div>
          <p>No notifications yet!</p>
        </div>
      )}
    </div>
  );

  return (
    <div className='modal__container'>
      <div className='modal modal--scrollable modal--has-close'>
        <button
          className='action-button action-button--close-button'
          onClick={dismiss}
        ></button>
        <p>Notifications:</p>

        <Scroll ScrollContent={NotificationsList}></Scroll>
      </div>
    </div>
  );
}
