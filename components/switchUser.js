import { getUser, setDetails } from "components/useData";
import { Scroll, ProfileImg } from "components";

export default function SwitchUser({ users, dismiss }) {
  const setUser = user => {
    setDetails(details => {
      return { ...details, currentUser: user };
    });
    dismiss();
  };

  const UserList = (
    <ul>
      {users.map(user => {
        let userObj = getUser(user);

        return (
          <div
            className="user-link"
            onMouseDown={() => setUser(user)}
            key={user}
          >
            <ProfileImg user={userObj} />
            <span>{userObj.name}</span>
          </div>
        );
      })}
    </ul>
  );

  return (
    <div className="modal__container">
      <div className="modal modal--scrollable modal--has-close">
        <button
          className="action-button action-button--close-button"
          onClick={dismiss}
        ></button>
        <p>Select user:</p>
        <Scroll ScrollContent={UserList}></Scroll>
      </div>
    </div>
  );
}
