import { Post } from 'components';
import { useState, useEffect, useContext } from 'react';
import { UserContext } from 'components/useData';
import post from './post';

//wtf is going on in this component
//

export default function PostList({ postlist, visibleNum }) {
  const userContext = useContext(UserContext);

  let [visibleList, setVisibleList] = useState([]);

  useEffect(() => {
    setVisibleList(postlist.slice(0, visibleNum));
  }, [postlist, visibleNum]);

  if (visibleList.length === 0) {
    return <div className='no-posts'>No posts found...</div>;
  }

  return (
    <div>
      {visibleList.map(post => (
        <Post toUpdate={userContext.toUpdate} key={post.id} post={post}></Post>
      ))}
    </div>
  );
}
