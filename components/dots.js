import { randomVec } from 'components/utils';
import dynamic from 'next/dynamic';

const DrawDots = dynamic(() => import('components/drawDots'), { ssr: false });

export default function Dots() {
  const defaultVelFactor = 0.0015;
  const defaultRange = 0.5;
  const dotNum = 150;

  const createDots = num =>
    new Array(num).fill().map((x, i) => ({
      k: i,
      point: randomVec(defaultRange),
      vel: randomVec(defaultVelFactor)
    }));

  const dotArr = createDots(dotNum);

  return (
    <div className='splash__bg'>
      <svg viewBox='-.5 -.5 1 1' preserveAspectRatio='xMidYMid slice'>
        <DrawDots defaultVelFactor={defaultVelFactor} dotArr={dotArr} />
      </svg>
    </div>
  );
}
