import { useState, useContext } from 'react';
import { makeComment, UserContext } from 'components/useData';

export default function AddComment({ post }) {
  const userContext = useContext(UserContext);
  const [text, setText] = useState('');
  const submitPost = event => {
    event.preventDefault();
    makeComment({ post, text });
    setText('');
    userContext.setToUpdate(post);
  };

  const updateText = event => {
    setText(event.target.value);
  };

  const submitIfEnter = event => {
    if (event.keyCode === 13 && !event.shiftKey) {
      submitPost(event);
    }
  };

  return (
    <form className='add-comment'>
      <textarea
        className='input-textarea emboss'
        placeholder='Write comment'
        type='textarea'
        name='text'
        value={text}
        onChange={updateText}
        onKeyUp={submitIfEnter}
      ></textarea>
      {text !== '' && (
        <button className='action-button' onClick={submitPost}>
          Post
        </button>
      )}
    </form>
  );
}
