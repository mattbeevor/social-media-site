import { UserContext } from 'components/useData';
import { useState, useEffect, useContext } from 'react';

export default function Modal() {
  const userContext = useContext(UserContext);
  const [open, setOpen] = useState(false);

  const dismiss = () => {
    setOpen(false);
  };

  const clickModalOuter = e => {
    if (e.target.closest('.modal')) {
      return;
    }
    dismiss();
  };

  useEffect(() => {
    ///when the modal has content, set open to true
    if (userContext.modal) {
      setOpen(true);
    }
  }, [userContext.modal]);

  useEffect(() => {
    ///when open is set to false, delay and then clear the model
    let timer;
    if (!open) {
      timer = setTimeout(() => {
        userContext.setModal(false);
      }, 250);
    }
    return () => clearTimeout(timer);
  }, [open]);

  if (!userContext.modal) {
    return null;
  }

  return (
    <div
      className={`modal__outer ${open ? '' : 'modal__outer--closing'}`}
      onMouseDown={userContext.modal && clickModalOuter}
    >
      {userContext.modal.func(dismiss)}
    </div>
  );
}
