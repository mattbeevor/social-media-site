import { UserLink } from 'components';
import { useState, useContext, useEffect } from 'react';
import Link from 'next/link';

import {
  setPostlikes,
  setCommentlikes,
  UserContext,
  getUser
} from 'components/useData';

export default function Likes({ likes, liked, type, id, post }) {
  const userContext = useContext(UserContext);
  let oneLike = likes.length === 1;
  let someLikes = likes.length > 1;
  let likeId = `${userContext.user}l${id}`;
  const [viewing, setViewing] = useState(false);
  const [rendering, setRendering] = useState(false);

  useEffect(() => {
    ///when open is set to false, delay and then clear the model
    let timer;
    if (!viewing) {
      timer = setTimeout(() => setRendering(false), 250);
    } else {
      setRendering(true);
    }
    return () => clearTimeout(timer);
  }, [viewing]);

  const addLike = setLikes => {
    setLikes(likes => ({
      ...likes,
      [likeId]: {
        user: `${userContext.user}`,
        dateTime: new Date().getTime(),
        target: id
      }
    }));
  };

  const removeLike = setLikes => {
    setLikes(likes => {
      let newlikes = { ...likes };
      delete newlikes[likeId];
      return newlikes;
    });
  };

  const toggleLiked = e => {
    e.preventDefault();
    const setLikes = type === 'post' ? setPostlikes : setCommentlikes;
    if (liked) {
      removeLike(setLikes);
    } else {
      addLike(setLikes);
    }
    userContext.setToUpdate(post);
  };

  const toggleLikes = () => {
    setViewing(v => !v);
  };

  const d = 'M 5 50 H 95 L 50 5 Z';

  return (
    <div className='likes'>
      <div className='likes__bar'>
        <button
          className={`like-button ${
            liked ? 'like-button--active' : 'like-button--inactive'
          }`}
          onMouseDown={toggleLiked}
        >
          <svg className='like-button__svg' viewBox='0 0 100 55'>
            <g className='like-button__bottom'>
              <path d={d}></path>
            </g>
            <g className='like-button__top'>
              <path className='like-button__top__outer' d={d}></path>
              <path className='like-button__top__inner' d={d}></path>
            </g>
          </svg>
        </button>
        {oneLike &&
          likes.map(getUser).map(userObj => (
            <span className='action-button' key={userObj.userName}>
              <span>Liked by </span>
              <Link
                legacyBehavior
                href='/user/[userName]'
                as={`/user/${userObj.userName}`}
              >
                <a>{userObj.name}</a>
              </Link>
            </span>
          ))}

        {someLikes && (
          <button
            className='action-button action-button--hover-1 show-likes'
            onMouseDown={toggleLikes}
          >
            Liked by {likes && likes.length ? likes.length : 0}{' '}
            <svg
              viewBox='0 0 18 18'
              className={`expand ${viewing ? 'expand--active' : ''}`}
            >
              <rect
                className='expand__1'
                x='0'
                y='8'
                width='18'
                height='2'
              ></rect>
              <rect
                className='expand__2'
                x='0'
                y='8'
                width='18'
                height='2'
              ></rect>
            </svg>
          </button>
        )}
      </div>
      <div
        className={`accordion ${someLikes && viewing && 'accordion--active'}`}
      >
        {rendering && (
          <div className={`likes--list`}>
            {likes.map(l => (
              <UserLink key={l} user={l}></UserLink>
            ))}
          </div>
        )}
      </div>
    </div>
  );
}
