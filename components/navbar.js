import Link from 'next/link';
import {
  UserContext,
  makeUser,
  getUser,
  otherUserList
} from 'components/useData';
import { useContext } from 'react';
import { Notifications, SwitchUser, ProfileImg } from 'components';

export default function NavBar({ notifications }) {
  const userContext = useContext(UserContext);

  const showExtraNavigation = () => {
    userContext.setModal({
      func: dismiss => (
        <div className='modal__container'>
          <div className='modal modal--has-close'>
            <a
              className='action-button action-button--hover-1'
              onMouseDown={() => {
                toggleDarkMode();
                dismiss();
              }}
            >
              {userContext.darkMode ? 'Light Mode' : 'Dark Mode'}
            </a>
            <br />
            <br />
            <button
              className='action-button action-button--close-button'
              onClick={dismiss}
            ></button>
            <a
              className='action-button action-button--hover-1'
              onClick={showSwitchUser}
            >
              Switch user
            </a>
            <br /> <br />
            <Link legacyBehavior href={`/`}>
              <a
                className='action-button action-button--hover-1'
                onClick={dismiss}
              >
                Logout
              </a>
            </Link>
          </div>
        </div>
      )
    });
  };

  const showSwitchUser = () => {
    userContext.setModal({
      func: dismiss => (
        <SwitchUser users={otherUserList} dismiss={dismiss}></SwitchUser>
      )
    });
  };

  const showNotifications = () => {
    let t = new Date().getTime();
    makeUser({ ...getUser(userContext.user), checked: t }, userContext.user);

    userContext.setModal({
      func: dismiss => (
        <Notifications
          notifications={notifications}
          dismiss={dismiss}
        ></Notifications>
      )
    });
  };

  const user = getUser(userContext.user);

  const toggleDarkMode = () => {
    userContext.setDarkMode(!userContext.darkMode);
  };

  return (
    <nav>
      <div className='main-column'>
        <div className='nav--column'>
          <Link legacyBehavior href={`/feed`}>
            <a>
              <span className='screen--gt-mobile shine'>Feed</span>
              <svg className='screen--lte-mobile nav-svg' viewBox='0 0 7 6'>
                <path d='M .8 6 V 3.75 H 0 L 3.5 0 L 7 3.75 H 6.2 V 6 Z'></path>
              </svg>
            </a>
          </Link>
          <Link legacyBehavior href={`/search`}>
            <a>
              <span className='screen--gt-mobile shine'>Search</span>
              <svg className='screen--lte-mobile nav-svg' viewBox='0 0 7 6'>
                <path d='M 5.95 6 L 2.72 2.2 L 6.2 5.75 Z'></path>
                <circle cx='2.72' cy='2.2' r='2.2'></circle>
              </svg>
            </a>
          </Link>
          <a className='nav-button-flex' onClick={showNotifications}>
            <span className='screen--gt-mobile shine'>Notifications</span>
            <svg className='screen--lte-mobile nav-svg' viewBox='0 0 7 6'>
              <path d='M 0 1 H 7 V 5 H 0 Z'></path>
              <path d='M 0 1.2 L 3.5 3.5 L 7 1.2'></path>
            </svg>
            <div className='notify'>
              <div>{notifications.unread}</div>
            </div>
          </a>
        </div>
        <div className='nav--column'>
          <Link
            legacyBehavior
            href='/user/[userName]'
            as={`/user/${getUser(userContext.user).userName}`}
          >
            <a className='profile-link nav-button-flex'>
              <span className='shine screen--gt-mobile shine'>Profile</span>{' '}
              <ProfileImg user={user} />
            </a>
          </Link>
          <button className='nav-toggle' onClick={showExtraNavigation}>
            <svg className='burger' viewBox='0 0 6 5'>
              <rect
                className='burger__1'
                x='0'
                y='0'
                width='6'
                height='1'
              ></rect>
              <rect
                className='burger__2'
                x='0'
                y='2'
                width='6'
                height='1'
              ></rect>
              <rect
                className='burger__3'
                x='0'
                y='4'
                width='6'
                height='1'
              ></rect>
            </svg>
          </button>
        </div>
      </div>
    </nav>
  );
}

//export default React.memo(NavBar, (oldProps,newProps)=>{
//  if((oldProps.notifications.length+newProps.notifications.length)===0){
//    return true
//  }
//  if(!oldProps.notifications[0]||!newProps.notifications[0]){
//    return false
//  }
//  let a = oldProps.notifications[0].dateTime ===  newProps.notifications[0].dateTime
//  return a
//});
