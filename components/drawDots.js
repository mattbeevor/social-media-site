import { rand, randomVec } from 'components/utils';
import { useState, useEffect, useRef } from 'react';

export default function drawDots({ defaultVelFactor, dotArr }) {
  let [dots, setDots] = useState(dotArr);

  useEffect(() => {
    const handleMouseMove = event => {
      const d = Math.max(window.innerWidth, window.innerHeight);
      const toCoord = client => client / d - 0.5;

      let xPixels = event.clientX;
      let yPixels = event.clientY;

      let xPixelScaleCenterOrigin = xPixels - window.innerWidth / 2;
      let yPixelScaleCenterOrigin = yPixels - window.innerHeight / 2;

      let xScaled = xPixelScaleCenterOrigin / d;
      let yScaled = yPixelScaleCenterOrigin / d;

      mouseRef.current = {
        x: xScaled,
        y: yScaled
      };
    };

    window.addEventListener('mousemove', handleMouseMove);

    return () => {
      window.removeEventListener('mousemove', handleMouseMove);
    };
  }, []);

  const requestRef = useRef();
  const mouseRef = useRef({ x: 0, y: 0 });

  const animate = () => {
    setDots(dots =>
      dots.map(dot => {
        let reset = false;

        let point = dot.point.map((prevCoord, i) => {
          let coord = prevCoord + dot.vel[i];

          if (i === 2) {
            if (reset) {
              return rand(0.5);
            }
          } else {
            if (coord > 0.5) {
              reset = true;
              return 0.5;
            }
            if (coord < -0.5) {
              reset = true;
              return -0.5;
            }
          }
          return coord;
        });
        return {
          point: reset
            ? [mouseRef.current.x, mouseRef.current.y, point[2]]
            : point,
          vel: reset ? randomVec(defaultVelFactor) : dot.vel,
          k: dot.k
        };
      })
    );

    requestRef.current = window.requestAnimationFrame(animate);
  };

  useEffect(() => {
    requestRef.current = window.requestAnimationFrame(animate);
    return () => window.cancelAnimationFrame(requestRef.current);
  }, []);

  return (
    <g>
      {dots.map(d => (
        <circle
          cx={d.point[0]}
          cy={d.point[1]}
          key={d.k}
          r={(Math.max(0.01, d.point[2]) + 2) * 0.001}
        ></circle>
      ))}
    </g>
  );
}
