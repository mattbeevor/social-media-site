import { simulate } from 'components';

export default function onLoad({ setVisibleNum }) {
  window.onbeforeunload = function (e) {
    return 'Reloading will remove any data you have set';
  };

  const scrollingPage = () => {
    if (!window.scrollTimeout) {
      window.scrollTimeout = window.setTimeout(() => {
        let screen = window.innerHeight;
        let scroll = window.pageYOffset;
        let page = document.body.offsetHeight;
        let remaining = page - (screen + scroll);

        if (remaining < screen * 4) {
          setVisibleNum(n => n + 1);
        }

        if (remaining < screen * 2) {
          setVisibleNum(n => n + 4);
        }

        if (remaining > screen * 10) {
          setVisibleNum(n => n - 1);
        }
        window.clearTimeout(window.scrollTimeout);
        window.scrollTimeout = false;
      }, 500);
    }
  };

  if (!window.simulateInterval) {
    window.simulateInterval = window.setInterval(simulate, 1000);
  }
  if (!window.onscroll) {
    window.onscroll = scrollingPage;
  }

  return <span></span>;
}
