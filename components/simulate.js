import {
  makeComment,
  makePost,
  makeCommentLike,
  makePostLike,
  generic,
  posts,
  comments,
  users,
  follows,
  commentlikes,
  postlikes,
  otherUserList
} from 'components/useData';
import { sortByRecent } from 'components/utils';
import {
  createPost,
  createComment,
  createPostLike,
  createCommentLike
} from 'components/randomContent';

function recent(obj, n) {
  let arr = Object.entries(obj)
    .map(x => {
      let id = x[0];
      let o = x[1];
      return { id, ...o };
    })
    .sort(sortByRecent)
    .slice(0, n);

  let res = {};

  for (let i = 0; i < arr.length; i++) {
    res[arr[i].id] = arr[i];
  }
  return res;
}

export default function simulate() {
  if (posts.length > 400) {
    return;
  }

  if (comments.length > 1000) {
    return;
  }

  const addRandomPost = () => {
    let p = createPost(generic.images, generic.posts, otherUserList, false);
    if (p) {
      makePost(p);
    }
  };

  const addRandomComment = () => {
    let c = createComment(
      generic.comments,
      posts,
      follows,
      otherUserList,
      true
    );
    if (c) {
      makeComment(c);
    }
  };

  const addRandomCommentNewPosts = () => {
    let c = createComment(
      generic.comments,
      recent(posts, 10),
      follows,
      otherUserList,
      true
    );
    if (c) {
      makeComment(c);
    }
  };

  const addRandomPostLike = () => {
    let c = createPostLike(posts, follows, postlikes, otherUserList, true);
    if (c) {
      makePostLike(c);
    }
  };

  const addRandomCommentLike = () => {
    let c = createCommentLike(
      comments,
      follows,
      commentlikes,
      otherUserList,
      true
    );
    if (c) {
      makeCommentLike(c);
    }
  };

  const addRandomPostLikeNewPosts = () => {
    let c = createPostLike(
      recent(posts, 2),
      follows,
      postlikes,
      otherUserList,
      true
    );
    if (c) {
      makePostLike(c);
    }
  };

  const addRandomCommentLikeNewComments = () => {
    let c = createCommentLike(
      recent(comments, 20),
      follows,
      commentlikes,
      otherUserList,
      true
    );
    if (c) {
      makeCommentLike(c);
    }
  };

  let options = [
    {
      weight: 0.1,
      action: addRandomPost
    },
    {
      weight: 0.5,
      action: addRandomComment
    },
    {
      weight: 1,
      action: addRandomCommentNewPosts
    },
    {
      weight: 0.1,
      action: addRandomPostLike
    },
    {
      weight: 0.1,
      action: addRandomCommentLike
    },
    {
      weight: 1,
      action: addRandomPostLikeNewPosts
    },
    {
      weight: 0.5,
      action: addRandomCommentLikeNewComments
    }
  ];

  const runOption = options => {
    let total = options.reduce((a, x) => a + x.weight, 0);
    let r = Math.random() * total;
    let counter = 0;
    let i = -1;

    let l = 0;

    while (counter < r && l < 100) {
      l++;
      i++;
      counter += options[i].weight;
    }
    options[i].action();
  };

  runOption(options);
}
