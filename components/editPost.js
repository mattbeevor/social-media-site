import { useState, useContext } from 'react';
import { EditImage } from 'components';
import { makePost, UserContext } from 'components/useData';

export default function EditPost({ postToEdit, setEditing }) {
  const userContext = useContext(UserContext);

  const [image, setImage] = useState((postToEdit && postToEdit.image) || '');
  const [text, setText] = useState((postToEdit && postToEdit.text) || '');

  const submitPost = event => {
    event.preventDefault();
    let key;
    if (postToEdit) {
      key = postToEdit.id;
    }
    key = makePost({ ...postToEdit, ...{ text, image } }, key);
    setText('');
    setImage('');
    if (setEditing) {
      setEditing(false);
    }
    userContext.setToUpdate(key);
  };

  const submitIfEnter = event => {
    event.preventDefault();
    if (event.keyCode === 13 && !event.shiftKey) {
      submitPost(event);
    }
  };

  return (
    <form className='edit-post' onSubmit={submitPost}>
      <textarea
        className='input-textarea emboss'
        placeholder='Write post'
        type='textarea'
        name='text'
        value={text}
        onChange={e => setText(e.target.value)}
        onKeyUp={submitIfEnter}
      />
      <EditImage image={image} setImage={setImage} />
      <div className='action-button__grid'>
        {Boolean(text || image) && (
          <button
            className='action-button action-button--bg-2 action-button--hover-3'
            onClick={submitPost}
          >
            Submit post
          </button>
        )}
        {setEditing && (
          <button className='action-button' onClick={() => setEditing(false)}>
            Cancel Update
          </button>
        )}
      </div>
    </form>
  );
}
