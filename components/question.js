export default function Question({ question, dismiss, action }) {
  return (
    <div className='modal'>
      <p className='modal__question'>{question}</p>
      <div className='modal__options'>
        <div className='action-button__grid'>
          <button
            className='action-button action-button--bg-2 action-button--hover-1'
            onClick={dismiss}
          >
            Cancel
          </button>
          <button
            className='action-button action-button--bg-2 action-button--hover-1'
            onClick={() => {
              action();
              dismiss();
            }}
          >
            Confirm
          </button>
        </div>
      </div>
    </div>
  );
}
