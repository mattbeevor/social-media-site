import { useRef } from 'react';
import { useEffect, useState } from 'react';

export default function Scroll({ ScrollContent }) {
  const scrollContent = useRef(null);
  const scrollBar = useRef(null);

  const toPx = n => n + 'px';

  let waiting = false;
  const barHeightOffset = 0;
  let offSetScroll = 0;
  let mouseIsDown = false;
  let [canScroll, setCanScroll] = useState(true);

  const onScroll = () => {
    if (waiting) {
      return;
    }

    setTimeout(() => (waiting = false), 2);

    let { offsetHeight, scrollTop, scrollHeight } = scrollContent.current;
    let proportion = offsetHeight / scrollHeight;
    scrollBar.current.style.top = toPx(scrollTop * proportion);
    scrollBar.current.style.height = toPx(
      offsetHeight * proportion + barHeightOffset
    );

    if (offsetHeight === scrollHeight) {
      setCanScroll(false);
    }

    waiting = true;
  };

  const setOffsetScroll = e => {
    offSetScroll = e.clientY - scrollBar.current.getBoundingClientRect().top;
  };

  const moveScrollBar = (e, isTouch) => {
    if (!isTouch && !mouseIsDown) {
      return;
    }

    e.stopPropagation();
    let { offsetHeight, scrollHeight } = scrollContent.current;
    let proportion = offsetHeight / scrollHeight;
    let scrollBarTop =
      e.clientY -
      scrollContent.current.getBoundingClientRect().top -
      offSetScroll;
    scrollBar.current.style.top = scrollBarTop;
    scrollContent.current.scrollTop = scrollBarTop / proportion;
  };

  const touch = func => {
    e => func(e.touches[0], true);
  };

  const mouseDown = () => {
    mouseIsDown = true;

    function mouseUpListener() {
      mouseIsDown = false;
      document.body.removeEventListener('mouseup', mouseUpListener);
      document.body.removeEventListener('mousemove', moveScrollBar);
    }

    document.body.addEventListener('mousemove', moveScrollBar);
    document.body.addEventListener('mouseup', mouseUpListener);
  };

  const scrollUp = () => {
    scrollContent.current.scrollTop = scrollContent.current.scrollTop + 90;
    onScroll();
  };

  const scrollDown = () => {
    scrollContent.current.scrollTop = scrollContent.current.scrollTop - 90;
    onScroll();
  };

  useEffect(onScroll);

  return (
    <div className='scroll-outer'>
      <div className='scroll-content' ref={scrollContent} onScroll={onScroll}>
        {ScrollContent}
      </div>
      <div className='scroll-fade--top'></div>
      <div className='scroll-fade--bottom'></div>
      <div className={canScroll ? 'scroll-bar' : 'scroll-bar-none'}>
        <div
          className='scroll-inner emboss'
          onMouseDown={e => {
            mouseDown();
            setOffsetScroll(e);
          }}
          onTouchStart={touch(setOffsetScroll)}
          onTouchMove={touch(moveScrollBar)}
          ref={scrollBar}
        >
          <div
            className='scroll-below'
            onTouchStart={scrollUp}
            onMouseDown={scrollUp}
          ></div>
          <div
            className='scroll-above'
            onTouchStart={scrollDown}
            onMouseDown={scrollDown}
          ></div>
        </div>
      </div>
    </div>
  );
}
