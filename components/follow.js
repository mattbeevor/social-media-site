import { setFollows, UserContext, getUser } from 'components/useData';
import { useState, useEffect, useContext } from 'react';
import { Question } from 'components';
import Link from 'next/link';

export default function Follow({ user }) {
  let [following, setFollowing] = useState();
  const userContext = useContext(UserContext);
  let followId = `${userContext.user}f${user}`;

  useEffect(() => {
    setFollowing(userContext.follows.includes(user));
  }, [user, userContext.follows]);

  function follow() {
    setFollows(follows => ({
      ...follows,
      [followId]: {
        user: userContext.user,
        target: user,
        dateTime: new Date().getTime()
      }
    }));
  }

  function followOptions() {
    if (userContext.user - user === 0) {
      userContext.setModal({
        func: dismiss => (
          <div className='modal'>
            <p className='modal__question'>Visit your profile?</p>
            <div className='modal__options' onClick={dismiss}>
              <div className='action-button__grid'>
                <button className='action-button'>Cancel</button>
                <Link
                  href='/user/[userName]'
                  as={`/user/${getUser(userContext.user).userName}`}
                  legacyBehavior
                >
                  <a className='action-button'>Confirm</a>
                </Link>
              </div>
            </div>
          </div>
        )
      });
    } else {
      const unfollowAction = () =>
        setFollows(follows => {
          let newfollows = { ...follows };
          delete newfollows[followId];
          return newfollows;
        });

      userContext.setModal({
        func: dismiss => (
          <Question
            question={`Unfollow ${getUser(user).name}?`}
            dismiss={dismiss}
            action={unfollowAction}
          ></Question>
        )
      });
    }
  }

  function toggle() {
    following ? followOptions() : follow();
  }

  return (
    <button
      className={`follow-toggle ${
        following ? '' : 'follow-toggle--notfollowing'
      }`}
      onMouseDown={toggle}
    >
      {following || userContext.user - user === 0 ? (
        <svg className='ellipsis' viewBox='0 0 32 32'>
          <circle cx='16' cy='16' r='2'></circle>
          <circle cx='8' cy='16' r='2'></circle>
          <circle cx='24' cy='16' r='2'></circle>
        </svg>
      ) : (
        'Follow'
      )}
    </button>
  );
}
