import { useRef } from 'react';

export default function AddImage({ image, setImage }) {
  const fileInput = useRef(null);

  const onSelectedImage = event => {
    let input = event.target;
    if (input.files && input.files[0]) {
      gotFile(input.files[0], () => (input.value = ''));
    }
  };

  const gotFile = (file, callback) => {
    var reader = new FileReader();
    reader.onload = function (e) {
      setImage(e.target.result);
      if (callback) {
        callback();
      }
    };
    reader.readAsDataURL(file);
  };

  const onDragEnter = e => {
    e.preventDefault();
  };

  const onDragOver = e => {
    e.preventDefault();
  };

  const removeImage = e => {
    setImage('');
  };

  const changeImage = e => {
    e.preventDefault();
    fileInput.current.click();
  };

  const onFileDrop = e => {
    e.preventDefault();
    if (e.dataTransfer.items) {
      for (var i = 0; i < e.dataTransfer.items.length; i++) {
        if (e.dataTransfer.items[i].kind === 'file') {
          gotFile(e.dataTransfer.items[i].getAsFile());
        }
      }
    } else {
      for (var i = 0; i < e.dataTransfer.files.length; i++) {
        gotFile(e.dataTransfer.files[i].name);
      }
    }
  };

  return (
    <div
      className='add-image emboss'
      onDragOver={onDragOver}
      onDrop={onFileDrop}
      onDragEnter={onDragEnter}
    >
      <p>Drag image here</p>
      <button
        className='action-button action-button--bg-2 action-button--hover-1'
        onClick={changeImage}
      >
        Select image from files
      </button>
      <input
        className='hidden-file-input'
        type='file'
        ref={fileInput}
        id='image'
        name='image'
        accept='image/png, image/jpeg'
        onChange={onSelectedImage}
      />

      <img src={image} />
      {Boolean(image) && (
        <p>
          <button className='detail' onClick={removeImage}>
            Remove image
          </button>
        </p>
      )}
    </div>
  );
}
