import {
  Ago,
  Comment,
  Likes,
  UserLink,
  EditPost,
  AddComment,
  Question
} from 'components';
import { useState, useContext } from 'react';
import { generic, UserContext, removePost } from 'components/useData';

export default function Post({ post }) {
  const userContext = useContext(UserContext);
  let image;

  let [editing, setEditing] = useState(false);

  if (post.image !== undefined) {
    image = (
      <img
        className='post-image'
        src={post.generic ? generic.images[post.image] : post.image}
      />
    );
  }

  const deletePost = event => {
    const action = () => {
      removePost(post.id);
      userContext.setToUpdate(false);
    };

    userContext.setModal({
      func: dismiss => (
        <Question
          question={'Delete this post?'}
          dismiss={dismiss}
          action={action}
        ></Question>
      )
    });
  };

  const editPost = () => {
    setEditing(true);
  };

  return (
    <div className='post emboss'>
      <UserLink user={post.user}></UserLink>
      {post.user === userContext.user ? (
        <div className='detail'>
          <Ago time={post.dateTime}></Ago> (
          <button onMouseDown={editPost}>&#8239;Edit Post&#8239;</button>|
          <button onMouseDown={deletePost}>&#8239;Delete Post&#8239;</button>)
        </div>
      ) : (
        <div className='detail'>
          <Ago time={post.dateTime}></Ago>
        </div>
      )}
      {!editing && image}
      {!editing && <p>{post.generic ? generic.posts[post.text] : post.text}</p>}
      {editing && (
        <EditPost postToEdit={post} setEditing={setEditing}></EditPost>
      )}
      <Likes
        liked={post.liked}
        likes={post.likes}
        type={'post'}
        id={post.id}
        post={post.id}
      ></Likes>
      {post.comments.map(comment => (
        <Comment
          key={comment.id}
          comment={comment}
          id={comment.id}
          post={post.id}
        ></Comment>
      ))}
      <AddComment post={post.id}></AddComment>
    </div>
  );
}
