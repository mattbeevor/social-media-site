import { UserLink, Likes, Ago, Question } from 'components';
import { useState, useContext } from 'react';
import {
  makeComment,
  generic,
  UserContext,
  removeComment
} from 'components/useData';

export default function cm({ comment, post, id }) {
  let body = (
    <p className='inputted-text'>
      {' '}
      {comment.generic ? generic.comments[comment.text] : comment.text}
    </p>
  );
  const userContext = useContext(UserContext);
  let [text, setText] = useState(comment.text);
  let [editing, setEditing] = useState(false);

  if (comment.user - userContext.user !== 0) {
    return (
      <div className='comment engrave' id={'comment-' + id}>
        <UserLink user={comment.user}></UserLink>

        <div className='detail'>
          <Ago time={comment.dateTime}></Ago>
        </div>
        {body}
        <Likes
          liked={comment.liked}
          likes={comment.likes}
          type={'comment'}
          id={comment.id}
          post={post}
        ></Likes>
      </div>
    );
  }

  const deletePost = () => {
    userContext.setModal({
      func: dismiss => (
        <Question
          question={'Delete this comment?'}
          dismiss={dismiss}
          action={() => removeComment(comment.id)}
        ></Question>
      )
    });
  };

  const editPost = () => {
    setEditing(true);
  };

  const saveEdit = event => {
    event.preventDefault();
    setEditing(false);
    makeComment({ ...comment, text }, comment.id);
  };

  const updateText = event => {
    setText(event.target.value);
  };

  return (
    <div className='comment engrave' id={'comment-' + id}>
      <UserLink user={comment.user}></UserLink>
      <div className='detail'>
        <Ago time={comment.dateTime}></Ago> (&#8239;
        <button onClick={editPost}>Edit Post</button>&#8239;|&#8239;
        <button onClick={deletePost}>Delete Comment</button>&#8239;)
      </div>
      {!editing && body}

      {editing && (
        <form>
          <input
            className='input-textarea'
            type='textarea'
            name='text'
            value={text}
            onChange={updateText}
          />
          <div className='action-button__grid'>
            <button className='action-button' onClick={() => setEditing(false)}>
              Cancel edit
            </button>
            {text && (
              <button className='action-button' onClick={saveEdit}>
                Save changes
              </button>
            )}
          </div>
        </form>
      )}

      <Likes
        likes={comment.likes}
        liked={comment.liked}
        type={'comment'}
        id={comment.id}
        post={post}
      ></Likes>
    </div>
  );
}

//export default React.memo(cm, (oldProps, newProps) => {
//  return (
//    newProps.comment.text === oldProps.comment.text &&
//    oldProps.comment.liked === newProps.comment.liked
//  );
//});
