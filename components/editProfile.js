import { useState, useEffect, useContext } from 'react';
import { EditImage } from 'components';
import { makeUser, UserContext } from 'components/useData';
import { generic } from 'components/useData';

export default function EditProfile({
  users,
  afterEdit,
  userToEdit,
  setEditing
}) {
  let start = {};

  if (userToEdit) {
    start = { ...userToEdit };
    if (userToEdit.generic) {
      start.bio = generic.bios[userToEdit.bio];
      start.image = generic.images[userToEdit.image];
    }
  }

  const [userName, setUserName] = useState(start.userName || '');
  const [name, setName] = useState(start.name || '');
  const [image, setImage] = useState(start.image || '');
  const [bio, setBio] = useState(start.bio || '');
  const [userNameIsUnique, setUserNameIsUnique] = useState(false);
  const [confirmed, setConfirmed] = useState(false);
  const userContext = useContext(UserContext);

  const submitProfile = event => {
    event.preventDefault();
    setConfirmed(true);

    let key = userContext.user;

    if (userToEdit) {
      makeUser(
        {
          ...userToEdit,
          ...{ userName, name, image, bio, generic: false }
        },
        key
      );
    } else {
      key = makeUser({
        ...userToEdit,
        ...{
          userName,
          name,
          image,
          bio,
          generic: false,
          checked: new Date().getTime()
        }
      });
    }
    afterEdit(userName, key);

    if (setEditing) {
      setEditing();
    }
  };

  useEffect(() => {
    let userNames = Object.entries(users)
      .filter(entry => entry[0] - userContext.user !== 0)
      .map(entry => entry[1].userName);

    setUserNameIsUnique(
      userName && userName.length > 0 && !userNames.includes(userName)
    );
  }, [userName, users]);

  const typeInUserNameField = e => {
    let text = e.target.value.toLowerCase().replace(/[^a-zA-Z0-9_\-]/g, '');
    setUserName(text);
  };

  return (
    <form className='create-profile' onSubmit={submitProfile}>
      <EditImage image={image} setImage={setImage} />
      <label>Full name:</label>
      <input
        className='input-textarea'
        placeholder='Full Name'
        type='text'
        name='text'
        value={name}
        onChange={e => setName(e.target.value)}
      />
      <label>Bio:</label>
      <input
        className='input-textarea '
        placeholder='About me'
        type='text'
        name='text'
        value={bio}
        onChange={e => setBio(e.target.value)}
      />
      <label>Username:</label>
      <input
        className='input-textarea'
        placeholder='Username'
        type='text'
        name='text'
        value={userName}
        onChange={typeInUserNameField}
      />
      {!userNameIsUnique && userName && !confirmed && (
        <p>User name already taken!</p>
      )}
      <div className='action-button__grid'>
        {Boolean(name && userNameIsUnique && setEditing) && (
          <button
            className='action-button action-button--bg-3 action-button--hover-2'
            onClick={submitProfile}
          >
            Update Profile
          </button>
        )}
        {Boolean(name && userNameIsUnique && !setEditing) && (
          <button
            className='action-button action-button--bg-3 action-button--hover-2'
            onClick={submitProfile}
          >
            Create Profile
          </button>
        )}
        {setEditing && (
          <button
            className='action-button action-button--bg-3 action-button--hover-2'
            onClick={() => setEditing(false)}
          >
            Cancel Update
          </button>
        )}
      </div>
    </form>
  );
}
