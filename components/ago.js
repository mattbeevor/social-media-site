export default function Ago({ time }) {
  const toText = time => {
    let ms = new Date().getTime() - time;

    let minutes = ms / 60000;
    let hours = minutes / 60;
    let days = hours / 24;
    let weeks = days / 7;
    let years = weeks / 52;

    if (years > 2) {
      return `${Math.round(years)} years ago`;
    }

    if (weeks > 2) {
      return `${Math.round(weeks)} weeks ago`;
    }

    if (days > 2) {
      return `${Math.round(days)} days ago`;
    }

    if (days > 1) {
      return `Yesterday`;
    }

    if (hours > 2) {
      return `${Math.round(hours)} hours ago`;
    }

    if (hours > 1) {
      return `An hour ago`;
    }

    if (minutes > 2) {
      return `${Math.round(minutes)} minutes ago`;
    }

    return `Just now`;
  };

  return <span className="ago">{toText(time)}</span>;
}
