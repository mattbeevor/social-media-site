import { generic } from "components/useData";

export default function profileImg({ user }) {
  const profileSrc = user.generic
    ? generic.images[user.image]
    : user.image === ""
    ? "/noimage.png"
    : user.image;

  return <img src={profileSrc} />;
}
