import { randomIndex, randomElement, randomTime } from "./utils.js";

export const createPost = (genImages, genPosts, users, timeRange) => ({
  generic: true,
  image: randomIndex(genImages),
  text: randomIndex(genPosts),
  dateTime: timeRange ? randomTime(timeRange) : new Date().getTime(),
  user: randomElement(Object.keys(users))
});

export const createFollow = (timeRange, users, follows) => {
  let user = randomElement(Object.keys(users));

  let targets = Object.keys(users).filter(potential => {
    if (potential === user) {
      return false;
    }
    if (follows[`${user}f${potential}`]) {
      return false;
    }
    return true;
  });

  if (targets.length === 0) {
    return false;
  }

  let target = randomElement(targets);

  let key = `${user}f${target}`;

  if (target === user || follows[key]) {
    return createFollow(r + 1, timeRange, users, follows);
  } else {
    return {
      key,
      obj: {
        user,
        target,
        dateTime: randomTime(timeRange)
      }
    };
  }
};

export const createComment = (genComments, posts, follows, users, now) => {
  let post = randomElement(Object.entries(posts));

  let commenters = Object.values(follows)
    .filter(follow => follow.target === post[1].user)
    .map(x => x.user);
  if (commenters.length === 0) {
    commenters = Object.keys(users);
  }

  return {
    generic: true,
    text: randomIndex(genComments),
    dateTime: now ? new Date().getTime() : randomTime(post[1].dateTime),
    user: randomElement(commenters),
    post: post[0]
  };
};

export const createCommentLike = (
  comments,
  follows,
  commentlikes,
  users,
  now
) => {
  let comment = randomElement(Object.entries(comments));
  let followers = Object.values(follows)
    .filter(follow => follow.target === comment[1].user)
    .map(x => x.user);

  if (followers.length === 0) {
    followers = Object.keys(users);
  }

  let likers = followers.filter(potential => {
    if (commentlikes[`${potential}l${comment}`]) {
      return false;
    }
    return true;
  });

  if (likers.length === 0) {
    return false;
  }

  let user = randomElement(likers);

  let key = `${user}l${comment[0]}`;

  return {
    key,
    obj: {
      user,
      target: comment[0],
      dateTime: now ? new Date().getTime() : randomTime(comment[1].dateTime)
    }
  };
};

export const createPostLike = (posts, follows, postlikes, users, now) => {
  let post = randomElement(Object.entries(posts));
  let followers = Object.values(follows)
    .filter(follow => follow.target === post[1].user)
    .map(x => x.user);

  if (followers.length === 0) {
    followers = Object.keys(users);
  }

  let likers = followers.filter(potential => {
    if (postlikes[`${potential}l${post}`]) {
      return false;
    }
    return true;
  });

  if (likers.length === 0) {
    return false;
  }

  let user = randomElement(likers);

  let key = `${user}l${post[0]}`;

  return {
    key,
    obj: {
      user,
      target: post[0],
      dateTime: now ? new Date().getTime() : randomTime(post[1].dateTime)
    }
  };
};
