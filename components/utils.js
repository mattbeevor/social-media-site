export const keysToIds = obj => {
  Object.entries(obj).forEach(entry => {
    if (typeof entry[1] === "object") {
      entry[1].id = entry[0];
    }
  });
  return obj;
};

export const sortByRecent = (a, b) =>
  parseInt(b.dateTime) - parseInt(a.dateTime);
export const sortIdByRecent = obj => (a, b) =>
  parseInt(obj[b].dateTime) - parseInt(obj[a].dateTime);
export const sortByOld = (a, b) => parseInt(a.dateTime) - parseInt(b.dateTime);
export const sortIdByOld = obj => (a, b) =>
  parseInt(obj[a].dateTime) - parseInt(obj[b].dateTime);
export const objectFilterArray = (obj, keyName, keyValues) =>
  entriesToIds(keyFilterObj(obj, keyName, keyValues));
export const objectFilterArraySortRecent = (obj, keyName, keyValues) =>
  entriesToIds(keyFilterObj(obj, keyName, keyValues)).sort(sortByRecent);

export const randomIndex = arr => Math.floor(Math.random() * arr.length);
export const randomElement = arr => arr[randomIndex(arr)];
export const randomTime = t => Math.floor(Math.random() * t);

export const createLookUp = (target, key, keyObj, sortFunc, otherKey) => {
  let lookup = {};
  Object.keys(keyObj).forEach(key => (lookup[key] = []));
  if (otherKey) {
    Object.entries(target).forEach(entry => {
      lookup[entry[1][key]].push(entry[1][otherKey]);
    });
  } else {
    Object.entries(target).forEach(entry => {
      lookup[entry[1][key]].push(entry[0]);
    });
  }
  if (sortFunc) {
    Object.entries(lookup).forEach(
      entry => (lookup[entry[0]] = entry[1].sort(sortFunc))
    );
  }
  return lookup;
};

export const createSelfLookUp = (obj, key) => {
  let lookup = {};
  Object.entries(obj).forEach(entry => {
    lookup[entry[1][key]] = entry[0];
  });
  return lookup;
};

export const rand = f => -f + Math.random() * f * 2;
export const randomVec = x => [rand(x), rand(x), rand(x)];
