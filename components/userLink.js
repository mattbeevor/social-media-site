import Link from 'next/link';
import { Follow, ProfileImg } from 'components';
import { getUser } from 'components/useData';

export default function UserLink({ user }) {
  let userObj = getUser(user);

  return (
    <div className='user-link'>
      <Link
        legacyBehavior
        href='/user/[userName]'
        as={`/user/${userObj.userName}`}
      >
        <a>
          <ProfileImg user={userObj} />
          <span>{userObj.name}</span>
        </a>
      </Link>
      <Follow user={user}></Follow>
    </div>
  );
}
