import faker from "faker";
import * as fs from "fs";

import {
  createPost,
  createFollow,
  createComment,
  createCommentLike,
  createPostLike,
} from "./components/randomContent.js";
import { randomTime } from "./components/utils.js";

faker.seed(144);

const usedUserNames = [];

const NUM = {
  GENERICPOSTS: 10,
  GENERICCOMMENTS: 30,
  GENERICBIOS: 10,
  USERS: 50,
  POSTS: 50,
  COMMENTS: 100,
  POSTLIKES: 300,
  COMMENTLIKES: 500,
  FOLLOWS: 1500,
};

const data = {
  generic: {
    comments: [],
    posts: [],
    images: [
      "/alan-king-KZv7w34tluA-unsplash.jpg",
      "/alex-munsell-auIbTAcSH6E-unsplash.jpg",
      "/alex-munsell-Yr4n8O_3UPc-unsplash.jpg",
      "/bogdan-lapadus-Z64bzMrSiy0-unsplash.jpg",
      "/casey-lee-awj7sRviVXo-unsplash.jpg",
      "/davide-cantelli-jpkfc5_d-DI-unsplash.jpg",
      "/kelsey-chance-tAH2cA_BL5g-unsplash.jpg",
      "/pure-virtual-wIz7bQlWdHo-unsplash.jpg",
      "/sam-mathews-wZjJxOx8FPI-unsplash.jpg",
      "/tina-xinia-WYM9XyOaZ20-unsplash.jpg",
      "/treddy-chen-F94kUL8aQGA-unsplash.jpg",
      "/zhipeng-ya-mgVBm8sqb1Y-unsplash.jpg",
      "/zhipeng-ya-qGpmVt9ivRI-unsplash.jpg",
      "/zhipeng-ya-ZHZHqWk_1Hs-unsplash.jpg",
    ],
    bios: [],
  },
  users: {},
  posts: {},
  comments: {},
  commentlikes: {},
  postlikes: {},
  follows: {},
  details: {
    lastcomment: -1,
    lastuser: -1,
    lastpost: -1,
    currentUser: "0",
  },
};

let c = 0;

let num = (name, i) => ++c % 99;
let nameArr = (name) => name.split(" ");
let fName = (name) => nameArr(name)[0];
let lName = (name) => nameArr(name)[nameArr(name).length - 1];
let fInitial = (name) => fName(name).charAt(0);
let lInitial = (name) => lName(name).charAt(0);
let uscore = () => "_";
let dash = () => "-";

let userNameFunctions = [
  [fName, uscore, lInitial],
  [fInitial, uscore, lName],
  [fName, uscore, lName],
  [fName, lName],
  [fName, lInitial],
  [fInitial, lName],
  [fName, uscore, lInitial, num],
  [fInitial, uscore, lName, num],
  [fName, uscore, lName, num],
  [fName, lName, num],
  [fName, num],
  [lName, num],
  [fName, lInitial, num],
  [fInitial, lName, num],
  [fInitial, uscore, lInitial, uscore, num],
  [fName, dash, lInitial],
  [fInitial, dash, lName],
  [fName, dash, lName],
  [fName, lName],
  [fName, lInitial],
  [fInitial, lName],
  [fName, dash, lInitial, num],
  [fInitial, dash, lName, num],
  [fName, dash, lName, num],
  [fName, lName, num],
  [fName, num],
  [lName, num],
  [fName, lInitial, num],
  [fInitial, lName, num],
  [fInitial, dash, lInitial, dash, num],
].map((nameElements) => (name, i) =>
  nameElements.map((f) => f(name, i)).join("")
);

const timeRange = -604800000;

let range = (n, f) => {
  for (let i = 0; i < n; i++) {
    f(i);
  }
};

const randomIndex = (arr) => Math.floor(Math.random() * arr.length);
const randomElement = (arr) => arr[randomIndex(arr)];

function toUserName(name, i) {
  let userName;
  userName = userNameFunctions[i % userNameFunctions.length](
    name.toLowerCase().replace(/[^a-zA-Z0-9 ]/g, ""),
    i
  );

  if (usedUserNames[userName]) {
    console.log(userName, "usedTwice");
    process.exit();
  }

  usedUserNames.push(userName);

  return userName;
}

function fakeUser(i) {
  let name = faker.name.findName();
  let userName = toUserName(name, i);
  data.details.lastuser += 1;
  data.users[data.details.lastuser] = {
    name,
    userName,
    generic: true,
    bio: randomIndex(data.generic.bios),
    image: randomIndex(data.generic.images),
    checked: randomTime(timeRange),
  };
}

function fakePost(i) {
  data.details.lastpost += 1;
  data.posts[data.details.lastpost] = createPost(
    data.generic.images,
    data.generic.posts,
    data.users,
    timeRange
  );
}

function fakeComment(i) {
  let comment = createComment(
    data.generic.comments,
    data.posts,
    data.follows,
    data.users
  );
  if (comment) {
    data.details.lastcomment += 1;
    data.comments[data.details.lastcomment] = comment;
  }
}

function fakeFollow(i) {
  let follow = createFollow(timeRange, data.users, data.follows);
  if (follow) {
    data.follows[follow.key] = follow.obj;
  }
}

function fakeCommentLike(i) {
  let like = createCommentLike(data.comments, data.follows, data.commentlikes);
  if (like) {
    data.commentlikes[like.key] = like.obj;
  }
}

function fakePostLike(i) {
  let like = createPostLike(data.posts, data.follows, data.postlikes);
  if (like) {
    data.postlikes[like.key] = like.obj;
  }
}

const genericPost = () => {
  data.generic.posts.push(faker.lorem.sentence());
};
const genericComment = () => {
  data.generic.comments.push(faker.lorem.sentence());
};
const genericBio = () => {
  data.generic.bios.push(faker.lorem.sentence());
};

range(NUM.GENERICPOSTS, genericPost);
range(NUM.GENERICCOMMENTS, genericComment);
range(NUM.GENERICBIOS, genericBio);

range(NUM.USERS, fakeUser);
range(NUM.POSTS, fakePost);
range(NUM.FOLLOWS, fakeFollow);
range(NUM.COMMENTS, fakeComment);
range(NUM.POSTLIKES, fakePostLike);
range(NUM.COMMENTLIKES, fakeCommentLike);

fs.writeFileSync("./public/data.json", JSON.stringify(data));
