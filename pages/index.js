import styles from 'styles/Home.module.css';
import Link from 'next/link';
import { Dots } from 'components';

export default function Home() {
  return (
    <main className='splash'>
      <Dots></Dots>
      <div className='splash__content'>
        <div className='splash__card'>
          <h1>Welcome, </h1>
          <div>
            <Link legacyBehavior href='/new'>
              <a>Create Profile</a>
            </Link>
            <Link legacyBehavior href='/feed'>
              <a>Use Random Profile</a>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
}
