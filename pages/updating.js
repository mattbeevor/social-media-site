import { useEffect } from "react";
import { NavBar } from "components";

export default function Updating({ data, router }) {
  useEffect(() => {
    document.querySelector(".profile-link").click();
  });

  return (
    <div>
      <NavBar notifications={data.notifications}></NavBar>
    </div>
  );
}
