import 'styles/globals.css';
import { useData, Modal } from 'components';
import { useState } from 'react';
import { UserContext } from 'components/useData';
import dynamic from 'next/dynamic';

const Onload = dynamic(() => import('components/Onload'), { ssr: false });

export default function App({ Component, pageProps, router }) {
  const data = useData(router);
  const [toUpdate, setToUpdate] = useState();
  const [modal, setModal] = useState();
  const [darkMode, setDarkMode] = useState(false);

  let cont = {
    user: data.details.currentUser,
    follows: data.following,
    toUpdate,
    setToUpdate,
    modal,
    setModal,
    darkMode,
    setDarkMode
  };

  let [visibleNum, setVisibleNum] = useState(5);

  return (
    <UserContext.Provider value={cont}>
      <Onload setVisibleNum={setVisibleNum} />
      <div className={darkMode ? 'mode mode--dark' : 'mode mode--light'}>
        <Component
          {...pageProps}
          data={data}
          router={router}
          visibleNum={visibleNum}
        />
        <Modal />
      </div>
    </UserContext.Provider>
  );
}
