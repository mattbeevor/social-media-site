import { NavBar, Post } from 'components';
import { useEffect, useContext } from 'react';
import { UserContext } from 'components/useData';

export default function PostView({ data, router }) {
  let { postList, notifications } = data;
  const userContext = useContext(UserContext);

  useEffect(() => {
    if (window && window.location.hash) {
      const comment = document.querySelector(window.location.hash);
      comment && comment.classList.add('target');
    }
  });

  useEffect(() => {
    postList[0] && userContext.setToUpdate(postList[0].id);
  }, [postList]);

  return (
    <div>
      <NavBar notifications={notifications} />
      <main>
        {postList[0] && (
          <Post toUpdate={userContext.toUpdate} post={postList[0]}></Post>
        )}
      </main>
    </div>
  );
}
