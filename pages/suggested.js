import { UserLink } from 'components';
import styles from 'styles/Home.module.css';
import { useState, useEffect, useContext } from 'react';
import Link from 'next/link';
import { UserContext, getUser } from 'components/useData';

export default function Suggested({ data }) {
  const { otherUserList, userFollowing } = data;
  const [list, setList] = useState();
  const userContext = useContext(UserContext);

  let userObj = getUser(userContext.user);

  useEffect(() => {
    if (!otherUserList) {
      return setList([]);
    }

    let list = otherUserList
      .slice(0, 10)
      .map(user => <UserLink key={user} user={user}></UserLink>);

    setList(list);
  }, [otherUserList, userFollowing]);

  return (
    <div className='suggested main-column'>
      <h1>
        Welcome {userObj.name}! Get started by following some people you might
        know!
      </h1>
      {list}
      <Link legacyBehavior href={`/feed`}>
        <button className='action-button action-button--bg-3 action-button--hover-2'>
          <a>Continue to my feed</a>
        </button>
      </Link>
    </div>
  );
}
