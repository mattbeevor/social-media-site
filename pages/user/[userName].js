import { NavBar, PostList, EditProfile, UserLink, EditPost } from 'components';
import { useState, useEffect, useContext } from 'react';
import { generic, UserContext } from 'components/useData';

export default function User({ data, router, visibleNum }) {
  let {
    usersByUserName,
    followsByUser,
    followsByTarget,
    postList,
    notifications,
    users
  } = data;

  let [user, setUser] = useState();
  let [current, setCurrent] = useState(false);
  let [followers, setFollowers] = useState([]);
  let [following, setFollowing] = useState([]);
  let [view, setView] = useState('posts');
  let [editing, setEditing] = useState(false);
  const userContext = useContext(UserContext);

  useEffect(() => {
    if (
      usersByUserName &&
      router.query.userName &&
      users &&
      userContext.user !== undefined
    ) {
      let userId = usersByUserName[router.query.userName];
      setCurrent(userContext.user - userId === 0);
      setUser(users[userId]);
      setFollowers(followsByTarget[userId]);
      setFollowing(followsByUser[userId]);
    }
  }, [usersByUserName, router, users, userContext.user]);

  useEffect(() => {
    setView('posts');
  }, [router.query.userName]);

  function editProfile() {
    setEditing(true);
  }

  function goToNewProfile(userName) {
    if (userName !== router.query.userName) {
      router.push('../updating');
    }
  }

  return (
    <div>
      <NavBar notifications={notifications} />
      <main>
        {editing && (
          <EditProfile
            users={users}
            afterEdit={goToNewProfile}
            userToEdit={user}
            setEditing={setEditing}
          ></EditProfile>
        )}

        {user && !editing && (
          <div className='profile'>
            <div className='profile__top-bar'>
              <img
                src={user.generic ? generic.images[user.image] : user.image}
              />
              {!editing && current && (
                <button className='detail' onClick={editProfile}>
                  Edit Profile
                </button>
              )}
            </div>
            <h1>{user.name} </h1>

            <p>{user.generic ? generic.bios[user.bio] : user.bio}</p>
            {current && <EditPost />}
            <div className='action-button__grid'>
              <button
                className={`action-button action-button--hover-2 action-button--active-3 ${
                  view === 'posts' ? 'action-button--active' : ''
                }`}
                onClick={() => setView('posts')}
              >
                Posts
              </button>
              <button
                className={`action-button action-button--hover-2 action-button--active-3 ${
                  view === 'followers' ? 'action-button--active' : ''
                }`}
                onClick={() => setView('followers')}
              >
                Followers ({followers.length})
              </button>
              <button
                className={`action-button action-button--hover-2 action-button--active-3 ${
                  view === 'following' ? 'action-button--active' : ''
                }`}
                onClick={() => setView('following')}
              >
                Following ({following.length})
              </button>
            </div>
          </div>
        )}

        {!editing && view === 'posts' && (
          <PostList
            postlist={postList}
            visibleNum={visibleNum}
            path={router.asPath}
          ></PostList>
        )}
        {!editing &&
          view === 'following' &&
          following.map(u => <UserLink key={u} user={u}></UserLink>)}
        {!editing &&
          view === 'followers' &&
          followers.map(u => <UserLink key={u} user={u}></UserLink>)}
      </main>
    </div>
  );
}
