import { EditProfile } from 'components';
import styles from 'styles/Home.module.css';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { setDetails } from 'components/useData';

export default function Home({ data }) {
  const router = useRouter();

  function suggest(userName, userId) {
    setDetails(details => ({ ...details, currentUser: userId }));
    router.push(`/suggested`);
  }

  return (
    <main className='new-user'>
      <h1>Create your profile</h1>

      <EditProfile users={data.users} afterEdit={suggest} />
      <button className='action-button action-button--bg-3 action-button--hover-2'>
        <Link legacyBehavior href={`/`}>
          <a>Cancel</a>
        </Link>
      </button>
    </main>
  );
}
