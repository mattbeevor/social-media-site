import { NavBar, EditPost, PostList } from 'components';
import styles from 'styles/Home.module.css';
import Link from 'next/link';

export default function Home({ data, visibleNum, router }) {
  let { postList, notifications } = data;

  return (
    <div>
      <NavBar notifications={notifications} />
      <main>
        <EditPost />
        <PostList
          postlist={postList}
          visibleNum={visibleNum}
          path={router.asPath}
        ></PostList>
        {!Boolean(postList.length) && (
          <p>
            No posts! Why not{' '}
            <Link legacyBehavior href={`/search`}>
              <a>
                <u>search for people to follow</u>
              </a>
            </Link>{' '}
            or{' '}
            <Link legacyBehavior href={`/suggested`}>
              <a>
                <u>check out our suggestions?</u>
              </a>
            </Link>{' '}
          </p>
        )}
      </main>
    </div>
  );
}
