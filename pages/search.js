import { NavBar, UserLink } from "components";
import styles from "styles/Home.module.css";
import { useState, useEffect } from "react";

export default function Search({ data }) {
  const { otherUserList, followsByUser, users, notifications } = data;
  const [query, setQuery] = useState("");
  const [list, setList] = useState();

  useEffect(() => {
    if (!otherUserList) {
      return setList([]);
    }

    if (!query) {
      otherUserList
        .slice(10)
        .map(user => <UserLink key={user} user={user}></UserLink>);
    }

    let matchString = user => users[user].name + " " + users[user].userName;
    let matchAny = new RegExp(query.split(" ").join("|"), "i");
    let usersMatching = otherUserList.map(user => ({
      user,
      matchString: matchString(user),
      matches: 0
    }));
    let filtered = usersMatching.filter(user =>
      matchAny.test(user.matchString)
    );
    let subqueries = query.split(" ");

    if (subqueries.length > 1) {
      subqueries.forEach(subquery => {
        let subqueryRegex = new RegExp(subquery, "i");
        filtered.forEach(
          user => (user.matches += subqueryRegex.test(user.matchString) ? 1 : 0)
        );
      });
      filtered.sort((a, b) => b.matches - a.matches);
    }

    let list = filtered.map(user => (
      <UserLink key={user.user} user={user.user}></UserLink>
    ));

    setList(list);
  }, [otherUserList, query, followsByUser]);

  const updateText = event => {
    setQuery(event.target.value);
  };

  return (
    <div>
      <NavBar notifications={notifications} />
      <main className="search">
        <input
          className="input-textarea"
          type="query"
          name="query"
          value={query}
          onChange={updateText}
        />
        {list}
      </main>
    </div>
  );
}
